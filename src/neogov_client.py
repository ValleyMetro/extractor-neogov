import json
# from collections.abc import Iterable

from kbc.client_base import HttpClientBase


JOBPOSTINGS_DEFAULT_COLS = ["additionalDomains", "companyId", "isDeleted", "mergeAudits", "portalId", "stateChanges"]


MAX_RETRIES = 10
BASE_URL = 'https://api.neogov.com/rest/'

# endpoints
JOBPOSTINGS_ALL = 'jobpostings/'


class NeoGovClient(HttpClientBase):
    """
    Basic HTTP client taking care of core HTTP communication with the API service.

    It exttends the kbc.client_base.HttpClientBase class, setting up the specifics for Hubspot service and adding
    methods for handling pagination.

    """

    def __init__(self):
        HttpClientBase.__init__(self, base_url=BASE_URL, max_retries=MAX_RETRIES, backoff_factor=0.3,
                                status_forcelist=(429, 500, 502, 504))

    def _get_results(self, endpoint, parameters, limit_attr, limit):
        """
        Generic getter method.

        :param endpoint:
        :param parameters:
        :param limit_attr:
        :param limit:
        :return:
        """

        parameters[limit_attr] = limit

        req = self.get_raw(self.base_url + endpoint, params=parameters)
        resp_text = str.encode(req.text, 'utf-8')
        req_response = json.loads(resp_text)

        yield req_response

    def get_jobpostings(self, recent=False, fields=None):

        limit = 1000
        parameters = {'perpage': limit}

        return self._get_results(JOBPOSTINGS_ALL, parameters, 'limit', limit)
