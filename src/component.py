# -*- coding: utf-8 -*-
##############################################################################
## File Name: component.py                                                  ##
## Created By: Thomas R. Adams                                              ##
## Last Modified: 2021-06-18                                                ##
##                                                                          ##
## Purpose: Connect to and download data from NeoGov.                       ##
##                                                                          ##
## Documentation URL: https://tinyurl.com/w8fonoo                           ##
##                                                                          ##
## Notes:                                                                   ##
##                                                                          ##
##############################################################################
import json
from pathlib import Path
import pandas as pd
import requests
import base64


# configuration variables
KEY_USERNAME = 'user_name'
KEY_PASSWORD = '#password'

passkey = KEY_USERNAME + ':' + KEY_PASSWORD

encoded = base64.b64encode(passkey.encode())

authHeader = '{} {}'.format('Basic', bytes.decode(encoded))

headers = {'Content-type': 'application/json',
           'Authorization': authHeader,
          }

payload = {'perpage': 1000,
           'pagenumber': 1,
          }

url = r'https://api.neogov.com/rest/jobpostings/'

r = requests.get(url = url, headers = headers, params = payload) # urlencode(payload)

print (r)

# Define Data Paths
DATA_FOLDER = Path('/data')

RESULT_FILE_PATH = DATA_FOLDER.joinpath('out/tables/output.csv')

config = json.load(open(DATA_FOLDER.joinpath('config.json')))

APP_VERSION = 'v0.0.21'

print('User Name: {}'.format(KEY_USERNAME))

print('Password: {}'.format(KEY_PASSWORD))

print('Running Version {}'.format(APP_VERSION))

df1 = pd.DataFrame(data={'col1': ['1', '2', '3', '4', '5'], 'col2': ['A', 'B', 'C', 'D', 'E']})

df1.to_csv(RESULT_FILE_PATH, index=False)
